              <div class="box box-info">
                <div class="box-header">
                  <i class="fa fa-pencil"></i>
                  <h3 class="box-title">CETAK TRANSAKSI</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>

                <div class="box-body">
                    <a class="btn btn-primary" target="_blank" href="<?php echo base_url('administrator/excel');?>"><i class="fa fa-print"></i> EXPORT KE EXCEL </a>
                </div>

              </div>
